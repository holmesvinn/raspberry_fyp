import socket
import time
import RPi.GPIO as GPIO
import SimpleMFRC522
from testing import Bluetoothctl

bl = Bluetoothctl()

GPIO.setwarnings(False);
success = 'success'
reader = SimpleMFRC522.SimpleMFRC522()
counter=1

UDP_IP_ADDRESS = "192.168.43.32"  
UDP_PORT_NO = 33333
clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serverSock.bind(('192.168.43.9', UDP_PORT_NO))

def readmethod():
        try:
            id,text = reader.read()
            print text
            text = text.replace(" ","")
            clientSock.sendto(text,(UDP_IP_ADDRESS, UDP_PORT_NO))   
        finally:
            GPIO.cleanup()

            

def writemethod():
        data,addr = serverSock.recvfrom(1024);
        data = data.replace(" ","");
        try:
            reader.write(data)
            print(success)
            clientSock.sendto(success,(UDP_IP_ADDRESS, UDP_PORT_NO))
        finally:
            GPIO.cleanup()
        
    

while True:
    data,addr = serverSock.recvfrom(1024);
    data = data.replace(" ","");
    print data
    if(data == 'track'):
        bl.start_scan()
        time.sleep(10)
        data,addr = serverSock.recvfrom(1024);
        data = data.replace(" ","");
        address = data
        print(data)

        while(1):
    
            time.sleep(5)
            print(str(address))
            result = str(bl.get_device_info(str(address)))
            print(result)
            rssi_val = int(result.find("RSSI"))
            rssi_val = result[rssi_val  : (rssi_val+9)]
            rssi_val = rssi_val + "dbm"
            print(rssi_val)
            
            clientSock.sendto(rssi_val,(UDP_IP_ADDRESS, UDP_PORT_NO))
            
            clientSock.sendto("test",(UDP_IP_ADDRESS, UDP_PORT_NO))
            data,addr = serverSock.recvfrom(1024);
            data = data.replace(" ","");
            print(data)
            
            
            if(data == 'read'):
                    readmethod()
                    
            if(data == 'write'):
                    writemethod()
                    
            if(data == 'scan'):
                    bl.stop_scan()
                    time.sleep(1)
                    bl.start_scan()
                    time.sleep(5)
            if(data == 'clear'):
                    break
                    
                

            
    if(data == 'read'):
        readmethod()
        
    if(data == 'write'):
        writemethod()
        
